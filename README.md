# python scripts to remote raspberry pi camera control

this repository contains scripts with necessary dependencies to control raspberry pi cameras remotely via ssh protocol.

### Necessary packages
* sys
* os
* time
* paramiko
---
paramiko installation: 

pip: `pip install paramiko`

conda: `conda install -c anaconda paramiko`

---

### Libraries functions help:

*class* SSH

    SSH class use the paramiko libreries to create a connection between raspberry pi and pc

    __ init __(self, rb)

      init function create the connection between all the raspberry pi and pc.
      
      Parameters:
        rb: [['ip','username','password'],[...],...,[...]]
                
          rb is a list with the info of the raspberry pi: ip, username and password.
          The info is required as string. 
                
        If connection fails, sys.exit() finishes python excecution. 
        Usually occurs when IP address of a raspberry change

    ex_comm(self,command):

        Executes a given command on the raspberrys command line.
        
        Parameters:
        
          command: The commmand to exectue requires as string.

    read_data(self,**kwarg):

        Read all the files into a given folder.
        
        Parameters:
           
          folder: The folder where the function is going to read the files, it's required as string.
        
        Return:
        
          output: a list (or multiple lists) of files.

    send(self,files):

        Send all the files in a list to all the raspberry pi.
        
        Parameters:
        
          files: This variable is a list with the files to send.

    recive(self,files,**kwargs):

        Recive a list of files that are given from the raspberry pi to pc.
        There are differentiated with a '_RaspberryNumber' at the end of the file name.
        The files are stored in a subfolder in the pc directory.
        
        Parameters:
        
          files:  List (or lists) of files, items have to be string. 
              If one list is given, the files are required to all the raspberry. 
              If multiple lists are given, each list corresponds to one raspberry and it's assined in the order it's given.
            
          folder2save(**kwargs): Create a folder to save the recived files
              If it's not given, a folder is created as:
                  folder to save = day_hour_minute_second (numerically)
              If folder exist:
                  try: folder to save = folderName_h_m_s, where: h:hour, m:minute, s:second
          
          rb_dir(**kwargs): folder where files are stored
          
        Return: 
      
          received_files: a list (or lists) of files received with the name that were stored
          
          folder: Folder where files were stored

    cam_capture(self,**kwargs):

        Execute a command that uses raspistill for capturing photographs.
        (https://www.raspberrypi.org/documentation/usage/camera/raspicam/raspistill.md)
             
        Parameters:
        
          ** It is necesary to declarate them explicity
          
          mode: This parameter determine the capture mode:
         
              mode = 1, continuous capture
              mode = 2, multiple single capture
              mode = 3, single campure

          format: This parameter determine the photos format
          
              avaliable formats are: jpg, png
              tiff format it's not recommended and these scripts don't support it
          
          (*optionals):
          The optional parameters are:
              - ISO
              - ss
              - t
              - tl
              - name
          
          for more information about it, read raspistill documentation: 
              https://www.raspberrypi.org/documentation/raspbian/applications/camera.md
          
          photos2take: This is just necesary with mode 2. The variable is the number of photos to take.
        
        return:
        
          photos: Return a list of photos stored in each raspberry pi
          
          rb_f2s: Return the folder name where photos were stored




