import paramiko as pr
import os
import sys
import time
import imaplib
import email
from threading import Thread
import warnings
warnings.filterwarnings(action='ignore',module='.*paramiko.*')

# This function create a bar status during file transfer

def printTotals(done, total):
    v = done/total
    print('\r{}{} Transfered {}%'.format('|'*round(v*20),'.'*round(20*(1-v)),round(v*100,2)),end='')

def read_mail(e_mail,password,rb_i):
    IP = []
    for i in range(rb_i):
        mail = imaplib.IMAP4_SSL('imap.gmail.com')
        mail.login(e_mail,password)
        mail.select('inbox')
        t,d = mail.search(None, 'Subject', 'Rpi{}'.format(i+1))    
        typ, dat = mail.fetch(bytes(d[0].split()[-1]), '(RFC822)' )
        msg = email.message_from_bytes(dat[0][1])    
        IP.append(msg['subject'][8:])
        print('IP raspberry {} = '.format(i), IP[i])
    return IP

# All the processes that require paramiko are created in this class

class SSH:

    '''
    SSH class use the paramiko libreries to create a connection between raspberry pi and pc  
    Paramiko installation: 
        pip:   pip install paramiko 
        conda: conda install -c anaconda paramiko
    '''

    def __init__(self,e_mail,password,rb_i,**kwargs):

        '''
        init function create the connection between all the raspberry pi and pc.
        Parameters:
            mail: 'mail@gmail.com'
            
            password: 'password'
            
            num: number of raspberry pi
            
            The script will read the e-mails from mail to find the IP of the raspberry pi in the subject of the last e-mail.
                       
        '''
        #rb = ['raspberry pi name', 'raspberry pi password']
        rb = ['pi','rbpy123']
            
        IP = read_mail(e_mail,password,rb_i)
        
        if 'PcDirectory' in kwargs:
            self.PcDirectory = kwargs['PcDirectory']  
        else:
            self.PcDirectory = os.getcwd()
        
        self.main = {}
        self.rb_names = []
        
        Problem = 0
        while True:
            try:
                for i in range(rb_i):
                    self.rb_names.append('rb_{}'.format(i+1))
                    self.main[self.rb_names[i]] = pr.SSHClient()
                    self.main[self.rb_names[i]].set_missing_host_key_policy(pr.AutoAddPolicy)
                    self.main[self.rb_names[i]].connect(IP[i],22,rb[0],rb[1])
                print('successful connection')
                break
            except:
                print('trying to connect... ')
                Problem += 1 
            if Problem == 5:
                sys.exit('Connection fails')

    def ex_comm(self,command):

        '''
        ex_comm function. Executes a given command on the raspberrys command line.
        Parameters:
            command: The commmand to exectue requires as string.
        '''

        for n in self.rb_names: self.main[n].exec_command(command)
        
    def read_data(self,**kwarg):

        '''
        read_data. Read all the files into a given folder.
        Parameters:
            folder: The folder where the function is going to read the files, it's required as string.
        
        Return  a list (or multiple lists) of files.
        '''

        o = []
        for n in self.rb_names:
            sftp = self.main[n].open_sftp()
            o.append(sftp.listdir(kwarg['Folder2read']))
            sftp.close(); time.sleep(0.2)
        return o

    def send(self,files):
        
        '''
        send. Send all the files in a list to all the raspberry pi.
        Parameters:
            files: This variable is a list with the files to send.
        '''
        for n in self.rb_names:
            sftp = self.main[n].open_sftp()
            for f in files:
                print('\nTransfering {} to {} \n'.format(f,n))
                sftp.put('{}/{}'.format(self.PcDirectory,f),f, callback = printTotals)
            sftp.close(); time.sleep(0.2)

    def recive(self,**kwargs):

        '''
        recive. Recive a list of files that are given from the raspberry pi to pc.
        There are differentiated with a '_RaspberryNumber' at the end of the file name.
        The files are stored in a subfolder in the pc directory.
        Parameters:
            files:  List (or lists) of files, items have to be string. 
                If one list is given, the files are required to all the raspberry. 
                If multiple lists are given, each list corresponds to one raspberry and it's assined in the order it's given.
            folder2save(*optional): Create a folder to save the recived files. 
                If it's not given, a folder is created as:
                    folder to save = day_hour_minute_second (numerically)
                If folder exist:
                    try: folder to save = folderName_h_m_s, where: h:hour, m:minute, s:second
            rb_dir: folder where files are stored
        Return: 
            received_files: a list (or lists) of files received with the name that were stored
            folder: Folder where files were stored.
        '''

        folder = kwargs['folder2save'] if 'folder2save' in kwargs else time.strftime('%d_%H_%M_%S')
        
        files = self.read_data(Folder2read = kwargs['rb_dir'])
        
        while True: 
            if not os.path.exists(self.PcDirectory+'\\'+folder):
                os.makedirs(self.PcDirectory+'\\'+folder)
                break
            else:
                folder += time.strftime('_%H_%M_%S')

        for i in range(len(self.rb_names)):
            os.makedirs(self.PcDirectory+'\\'+folder+'\\'+kwargs['PhotosName']+str(i))
            temp_files = files[i] if type(files[0]) == list else files
            sftp = self.main[self.rb_names[i]].open_sftp()
            for f in temp_files:
                if '.Trash-1000' not in f:
                    f2 = kwargs['NameFormat']
                    if 'Name' in f2:
                        f2 = f2.replace('Name', kwargs['PhotosName'])
                    if '0' in f2:
                        num0 = f2.count('0')
                        f2= f2.replace('0'*num0,''.join([n for n in f if n.isdigit()]))
                    if 'ext' in f2:
                        f2 = f2.replace('ext',f[-3:])
                    if 'rbNUM' in f2:
                        f2 = f2.replace('rbNUM', '{:02}'.format(i))
                    if 'rbID' in f2:
                        f2 = f2.replace('rbID','1{:02}'.format(i))  
                        
                    #f2 = '{}_{}{}'.format(f[:f.index('.')],i,f[f.index('.'):]) if '.' in f else '{}_{}'.format(f,i)
                    print('\n\nTransfering {} from {}'.format(f,self.rb_names[i]))
                    sftp.get('{}/{}'.format(kwargs['rb_dir'],f),'{}\\{}\\{}\\{}'.format(self.PcDirectory,folder,kwargs['PhotosName']+str(i),f2), callback = printTotals)
            sftp.close(); time.sleep(0.1)

        received_files = os.listdir(self.PcDirectory+'\\'+folder)
        
        return received_files,self.PcDirectory+'\\'+folder

    def CamCapture(self,**kwargs):

        '''
        cam_capture. Execute a command that uses raspistill for capturing photographs.
            (https://www.raspberrypi.org/documentation/usage/camera/raspicam/raspistill.md)
             
        Parameters: 
            ** It is necesary to declarate them explicity as ex: ISO = 100 
            
            mode: This parameter determine the capture mode:
                mode = 1, continuous capture
                mode = 2, multiple single capture
                mode = 3, single campure

            format: This parameter determine the photos format
                avaliable formats are: jpg, png
                tiff format it's not recommended and these scripts don't support it
            
            (*optionals):
            The optional parameters are:
                - ISO
                - ss
                - t
                - tl
                - PhotosName
            
            for more information about it, read raspistill documentation: 
                https://www.raspberrypi.org/documentation/raspbian/applications/camera.md
            
            photos2take: This is just necesary with mode 2. The variable is the number of photos to take.
        return:
            photos: Return a list of photos stored in each raspberry pi
            rb_f2s: Return the folder name where photos were stored
        '''

        #raspistill initialization and optional variables 
        command = 'raspistill -bm -q 100 -e jpg -th none -n --exif off -cfx 128:128 '
        
        for opt in ['ISO','ss','t','tl']:
            if opt in kwargs: command += '-{} {} '.format(opt, kwargs[opt])
            
        command += '-o /dev/shm/{}'.format(kwargs['PhotosName'] if 'PhotosName' in kwargs else 'im')
        
        rb_f2s = time.strftime('%Y_%m_%d_%H_%M_%S')
        self.ex_comm('mkdir {}'.format(rb_f2s))
        input('Press enter to start ... ')

        if kwargs['mode'] == 1: # mode 1: continuous capture
            self.ex_comm('{}%04d.{}'.format(command,kwargs['format']))
            if kwargs['t'] == 0: 
                time.sleep(5)
                print('Press Ctrl + C to stop... ')
                try:
                    while True:
                        data = self.read_data(Folder2read = '/dev/shm')
                        for i in range(len(self.rb_names)):
                            data[i].sort()
                            if len(data[i]) > 20:
                                for d in data[i][:10]:
                                    stdin, stdout, stderr = self.main[self.rb_names[i]].exec_command('mv /dev/shm/{} {}'.format(d,rb_f2s))
                                    stdout.readlines()
                                if '.Trash-1000' in data[i][:3]:
                                    stdin, stdout, stderr = self.main[self.rb_names[i]].exec_command('rm -rf /dev/shm/.Trash-1000')
                                    stdout.readlines()
                except KeyboardInterrupt:
                    self.ex_comm('killall raspistill')
            else: print('wait until photos were taken ... '); time.sleep(kwargs['t']/1000+5)
        elif kwargs['mode'] == 2: # mode 2: multiple single capture
            for i in range(kwargs['photos2take']):
                input('Press enter to capture image {:04}'.format(i))
                self.ex_comm('{}{:04}.{}'.format(command,i,kwargs['format']))
                while True:
                    if all(len(read) == i+1 for read in self.read_data(folder = '/dev/shm')):
                        break
        elif kwargs['mode'] == 3: # mode 3: single capture
            self.ex_comm('{}.{}'.format(command,kwargs['format']))
            while True:
                if all(len(read) == 1 for read in self.read_data(folder = '/dev/shm')):
                    break
        self.ex_comm('mv /dev/shm/* {}'.format(rb_f2s))

        return rb_f2s     